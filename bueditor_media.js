/**
 * @file: Media injection handling for BUEditor
 *
 * Drupal.media.BUE.insertMedia
 *   Callback function to be called from a custom BUEditor button.
 */
(function ($) {
  namespace('Drupal.media.BUE');
  Drupal.media.BUE = {
      
    /*
     * Callback function to be called from a custom BUEditor button.
     * 
     * Invokes the media browser and inserts the markup for the selected
     * media into the editor window.
     * 
     * Add the following to a BUEditor custom button:
     *   js: Drupal.media.BUE.insertMedia(E);
     * 
     * @param {object}
     *   The BUEditor instance to act on
     */
    insertMedia: function(E) {
      // Invoke mediaBrowser with callback to handle selected files
      Drupal.media.popups.mediaBrowser(function(mediaFiles) {
        var file = mediaFiles[0];

        // Get fid from selected media
        var tagContent = {
            "type": 'media',
            "fid" : file.fid,
            "attributes": {}
        };
        // Invoke style selector with the first selected file
        Drupal.media.popups.mediaStyleSelector(file, function(media) {

          // Iterate over img tags attributes and add them
          jQuery.each(
              jQuery(media.html)[0].attributes,
              function(i, a) {
                tagContent.attributes[a.name] = a.value;
              }
          );
          // Populate remaining media details
          tagContent.view_mode = media.type;
          tagContent.attributes.alt = media.options.alt;

          // Remove elements from attribs using the blacklist
          for (var blackList in Drupal.settings.media.blacklist) {
            delete tagContent.attributes[Drupal.settings.media.blacklist[blackList]];
          }

          // Generate the JSON media tag
          var inline = '[[' + JSON.stringify(tagContent) + ']]';
          E.replaceSelection(inline);
        }, {});
      });

    }
  };
}(jQuery));
